<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarResource;
use App\Http\Resources\ErrorResource;
use App\Http\Resources\MessageResource;
use App\Models\Car;
use App\Models\Variant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CarController extends Controller
{
    public function index()
    {
        $cars = Auth::user()->cars()->paginate(100);
        return CarResource::collection($cars);
    }

    public function one(Request $request, int $id)
    {
        $cars = $request->user()->cars()->where(['id' => $id])->get();
        return CarResource::collection($cars);
    }

    public function new(Request $request, ?int $id = null)
    {
        $data = $request->only(['variant_id', 'year', 'mileage', 'color']);
        $data['user_id'] = $request->user()->id;

        if ($id) $data['variant_id'] = $id;
        if (!isset($data['variant_id'])) {
            return ErrorResource::collection([[
                'error' => '1001',
                'message' => 'Не указаны обязательные поля'
            ]]);
        }
        $variant = Variant::find($data['variant_id']);
        if (!$variant) {
            return ErrorResource::collection([[
                'error' => '1003',
                'message' => 'Не верно указана марка'
            ]]);
        }
        $data['brand_id'] = $variant->brand->id;
        $car = Car::create($data);
        if ($car) {
            return CarResource::collection(Car::where('id', $car->id)->get());
        } else {
            return ErrorResource::collection([
                [
                    'error' => '1004',
                    'message' => 'Не удалось добавить автомобиль'
                ]
            ]);
        }
    }

    public function delete(?int $car = null)
    {
        $car = Auth::user()->cars()->find($car);
        if ($car) {
            $car->delete();
        } else {
            return ErrorResource::collection([
                [
                    'error' => '1006',
                    'message' => 'Не возможно удалить сущность'
                ]
            ]);
        }
        return MessageResource::collection([
            [
                'result' => 'successful',
                'message' => 'Сущность удалена'
            ]
        ]);
    }

    public function update(Request $request, int $id)
    {
        $data = $request->only(['variant_id', 'year', 'mileage', 'color']);
        if (!$id) {
            return ErrorResource::collection([[
                'error' => '1001',
                'message' => 'Не указаны обязательные поля'
            ]]);
        } else {
            $cars = $request->user()->cars();
            $car = $cars->find($id);
            if($car){
                $car->color = $data['color'] ?? $car->color;
                $car->year = $data['year'] ?? $car->year;
                $car->mileage = $data['mileage'] ?? $car->mileage;
                $car->variant_id = $data['variant_id'] ?? $car->variant_id;
                $brand = Variant::find($car->variant_id)->brand;
                $car->brand_id = $brand->id;

                if ($car->save()) {
                    return CarResource::collection($cars->where('id', $car->id)->get());
                } else {
                    return ErrorResource::collection([
                        [
                            'error' => '9000',
                            'message' => 'Неизвестная ошибка'
                        ]
                    ]);
                }
            } else {
                return ErrorResource::collection([
                    [
                        'error' => '1007',
                        'message' => 'Доступ запрещен'
                    ]
                ]);
            }
        }
    }
}
