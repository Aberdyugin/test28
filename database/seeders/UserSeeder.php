<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::factory(10)->create();
        $users = User::all();
        foreach ($users as $user){
            echo "User:\n";
            $token = $user->createToken('api')->plainTextToken;
            printf("\t%s: %s\n\n", $user->email, $token);
        }
    }
}
