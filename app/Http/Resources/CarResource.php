<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class CarResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        return [
            'meta'=>[
                'href'=>route('oneCar',$this->id)
            ],
            'brandMeta'=>[
                'href'=>route('oneBrand',$this->brand_id)
            ],
            'variantMeta'=>[
                'href'=>route('oneVariant',$this->variant_id)
            ],
            'id'=> $this->id,
            'brand_id'=> $this->brand_id,
            'variant_id'=> $this->variant_id,
            'user_id'=> $this->user_id,
            'year'=>$this->year,
            'mileage'=>$this->mileage,
            'color'=>$this->color
        ];
    }
}
