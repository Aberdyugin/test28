<?php

namespace Database\Seeders;

use App\Models\Brand;
use App\Models\Car;
use App\Models\User;
use App\Models\Variant;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        Brand::factory(5)->create();
        Variant::factory(20)->create();
        Car::factory(50)->create();

        $users = User::all();
        foreach ($users as $user){
            echo "User:\n";
            $token = $user->createToken('api')->plainTextToken;
            printf("\t%s: %s\n\n", $user->email, $token);;
        }

        $cars = Car::all();
        foreach ($cars as $car){
            $car->user_id = rand(1,10);
            $car->save();
        }
    }
}
