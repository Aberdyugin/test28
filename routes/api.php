<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CarController;
use App\Http\Controllers\VariantController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/')->group(function () {
    Route::prefix('/v1.0')->group(function () {
        Route::prefix('/entity')->group(function () {
            Route::prefix('/brand')->group(function () {
                Route::get('/', [BrandController::class, 'index']);
                Route::get('/{id}', [BrandController::class, 'one'])->name('oneBrand');
            });
            Route::prefix('/variant')->group(function () {
                Route::get('/', [VariantController::class, 'index']);
                Route::get('/{id}', [VariantController::class, 'one'])->name('oneVariant');
                Route::middleware('auth:sanctum')->group(function () {
                    Route::put('/{id?}', [CarController::class, 'new'])->name('putNewCar');
                });
            });
            Route::prefix('/car')->group(function () {
                Route::middleware('auth:sanctum')->group(function () {
                    Route::get('/', [CarController::class, 'index']);
                    Route::get('/{id}', [CarController::class, 'one'])->name('oneCar');
                    Route::post('/', [CarController::class, 'new'])->name('postNewCar');
                    Route::delete('/{car?}', [CarController::class, 'delete'])->name('deleteCar');
                    Route::put('/{id}', [CarController::class, 'update'])->name('updateCar');
                });
            });
        });
    });
});
