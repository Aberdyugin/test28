<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use JsonSerializable;

class VariantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array|Arrayable|JsonSerializable
     */
    public function toArray($request): array|JsonSerializable|Arrayable
    {
        return [
            'meta'=>[
                'href'=>route('oneVariant', $this->id)
            ],
            'brandMeta'=>[
                'href'=>route('oneBrand', $this->brand_id)
            ],
            'id' => $this->id,
            'name' => $this->name,
            'brand_id'=>$this->brand_id
        ];
    }
}
