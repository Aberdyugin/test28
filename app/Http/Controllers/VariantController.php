<?php

namespace App\Http\Controllers;

use App\Http\Resources\VariantResource;
use App\Models\Variant;
use Illuminate\Http\Request;

class VariantController extends Controller
{
    public function index()
    {
        $variants = Variant::all();
        return VariantResource::collection($variants);
    }

    public function one(int $id)
    {
        $variants = Variant::where('id',$id)->get();
        return VariantResource::collection($variants);
    }

}
