<?php

namespace Database\Factories;

use App\Models\Variant;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $id = rand(1, 20);
        $variant = Variant::find($id);
        $brand = $variant->brand;
        $year = $this->faker->year('-1 year');

        return [
            'brand_id' => $brand->id,
            'variant_id' => $id,
            'user_id' => rand(1, 10),
            'year' => $year,
            'color' => $this->faker->colorName(),
            'mileage' => rand(50, 300) * 100 * date('Y') - $year
        ];
    }
}
